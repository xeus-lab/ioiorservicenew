package xyz.stepsecret.ioiorservicenew;

/**
 * Created by Aiii_ on 7/10/2559.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import ioio.lib.api.AnalogInput;
import ioio.lib.api.DigitalOutput;
import ioio.lib.api.IOIO;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;
import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOService;

public class BoundService extends IOIOService {

    private static String LOG_TAG = "BoundService";
    private IBinder mBinder = new MyBinder();

    private final int CH1_PIN = 34;
    private final int CH2_PIN = 35;
    private final int CH3_PIN = 36;
    private final int CH4_PIN = 37;
    private final int CH5_PIN = 38;
    private final int CH6_PIN = 39;

    private final int buffer_size = 32;
    private int count_size = 0;

    private float[] buff_ch1 = new float[buffer_size];
    private float[] buff_ch2 = new float[buffer_size];
    private float[] buff_ch3 = new float[buffer_size];
    private float[] buff_ch4 = new float[buffer_size];
    private float[] buff_ch5 = new float[buffer_size];
    private float[] buff_ch6 = new float[buffer_size];

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(LOG_TAG, "in onCreate");

        Log.e(" MyService "," Service onCreate");




    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.e(LOG_TAG, "in onBind");
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        Log.e(LOG_TAG, "in onRebind");
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.e(LOG_TAG, "in onUnbind");
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(LOG_TAG, "in onDestroy");


    }

    @Override
    protected IOIOLooper createIOIOLooper() {
        return new BaseIOIOLooper() {
            private DigitalOutput led_;
            private DigitalOutput led_22;
            private AnalogInput CH1_IN;
            private AnalogInput CH2_IN;
            private AnalogInput CH3_IN;
            private AnalogInput CH4_IN;
            private AnalogInput CH5_IN;
            private AnalogInput CH6_IN;
            @Override
            protected void setup() throws ConnectionLostException,
                    InterruptedException {
                Log.e(" MyService "," setup");
                led_ = ioio_.openDigitalOutput(IOIO.LED_PIN);
                led_22 = ioio_.openDigitalOutput(22, true);

                 CH1_IN = ioio_.openAnalogInput(CH1_PIN);
                 CH2_IN = ioio_.openAnalogInput(CH2_PIN);
                 CH3_IN = ioio_.openAnalogInput(CH3_PIN);
                 CH4_IN = ioio_.openAnalogInput(CH4_PIN);
                 CH5_IN = ioio_.openAnalogInput(CH5_PIN);
                 CH6_IN = ioio_.openAnalogInput(CH6_PIN);

                showVersions(ioio_, "Incompatible firmware version!");
            }
            @Override
            public void disconnected() {

                Log.e("IOIO "," disconnected");

                Notify(0,0,0,0,0,0);
                showDisconnect();

            }


            @Override
            public void incompatible() {


            }


            @Override
            public void loop() throws ConnectionLostException,
                    InterruptedException {
                /*led_.write(false);
                led_22.write(false);
                Thread.sleep(10);
                led_.write(true);
                led_22.write(true);
                Thread.sleep(10);
                */

                led_.write(false);
                //Thread.sleep(2);
                led_.write(true);

                float CH1_Volts = CH1_IN.getVoltage();
                float CH2_Volts = CH2_IN.getVoltage();
                float CH3_Volts = CH3_IN.getVoltage();
                float CH4_Volts = CH4_IN.getVoltage();
                float CH5_Volts = CH5_IN.getVoltage();
                float CH6_Volts = CH6_IN.getVoltage();

                /*if(CH1_Volts < 1.5)
                {
                    CH1_Volts = CH1_Volts * -1;
                }
                if(CH2_Volts < 1.5)
                {
                    CH2_Volts = CH2_Volts * -1;
                }
                if(CH3_Volts < 1.5)
                {
                    CH3_Volts = CH3_Volts * -1;
                }
                if(CH4_Volts < 1.5)
                {
                    CH4_Volts = CH4_Volts * -1;
                }
                if(CH5_Volts < 1.5)
                {
                    CH5_Volts = CH5_Volts * -1;
                }
                if(CH6_Volts < 1.5)
                {
                    CH6_Volts = CH6_Volts * -1;
                }
                */

                buff_ch1[count_size] = CH1_Volts;
                buff_ch2[count_size] = CH2_Volts;
                buff_ch3[count_size] = CH3_Volts;
                buff_ch4[count_size] = CH4_Volts;
                buff_ch5[count_size] = CH5_Volts;
                buff_ch6[count_size] = CH6_Volts;

                count_size++;

                if(count_size >= buffer_size)
                {
                    count_size = 0;

                    showLoop(buff_ch1, buff_ch2, buff_ch3, buff_ch4, buff_ch5, buff_ch6);
                }

                /*Log.e("Bound Service ",
                         " CH1 : "+CH1_Volts
                        +", CH2 : "+CH2_Volts
                       +", CH3 : "+CH3_Volts
                        +", CH4 : "+CH4_Volts
                        +", CH5 : "+CH5_Volts
                        +", CH6 : "+CH6_Volts);
                */

                Notify(CH1_Volts, CH2_Volts, CH3_Volts, CH4_Volts, CH5_Volts, CH6_Volts);


                //Log.e(" MyService "," loop" +"");
            }
        };
    }

    private void showVersions(IOIO ioio, String status) {

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("ioio_connect");
        broadcastIntent.putExtra("Status", status);
        broadcastIntent.putExtra("IOIOLib", ioio.getImplVersion(IOIO.VersionType.IOIOLIB_VER));
        broadcastIntent.putExtra("Application", ioio.getImplVersion(IOIO.VersionType.APP_FIRMWARE_VER));
        broadcastIntent.putExtra("Bootloader", ioio.getImplVersion(IOIO.VersionType.BOOTLOADER_VER));
        broadcastIntent.putExtra("Hardware", ioio.getImplVersion(IOIO.VersionType.HARDWARE_VER));
        sendBroadcast(broadcastIntent);

        String g = ioio.getImplVersion(IOIO.VersionType.IOIOLIB_VER);
        String g1 = ioio.getImplVersion(IOIO.VersionType.APP_FIRMWARE_VER);
        String g2 = ioio.getImplVersion(IOIO.VersionType.BOOTLOADER_VER);
        String g3 = ioio.getImplVersion(IOIO.VersionType.HARDWARE_VER);
    }

    private void showDisconnect()
    {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("ioio_disconnect");
        sendBroadcast(broadcastIntent);
    }

    private void showLoop(float[] CH1_Volts, float[] CH2_Volts, float[] CH3_Volts, float[] CH4_Volts
            , float[] CH5_Volts, float[] CH6_Volts)
    {
        Intent broadcastIntent = new Intent();

        broadcastIntent.setAction("ioio_loop");
        broadcastIntent.putExtra("CH1_Volts", CH1_Volts);
        broadcastIntent.putExtra("CH2_Volts", CH2_Volts);
        broadcastIntent.putExtra("CH3_Volts", CH3_Volts);
        broadcastIntent.putExtra("CH4_Volts", CH4_Volts);
        broadcastIntent.putExtra("CH5_Volts", CH5_Volts);
        broadcastIntent.putExtra("CH6_Volts", CH6_Volts);

        sendBroadcast(broadcastIntent);
    }

    public class MyBinder extends Binder {
        BoundService getService() {
            return BoundService.this;
        }
    }

    private void Notify(float CH1_Volts, float CH2_Volts, float CH3_Volts, float CH4_Volts, float CH5_Volts, float CH6_Volts){

        // Create the style object with InboxStyle subclass.
        NotificationCompat.InboxStyle notiStyle = new NotificationCompat.InboxStyle();

        notiStyle.addLine("CH1 : "+CH1_Volts);
        notiStyle.addLine("CH2 : "+CH2_Volts);
        notiStyle.addLine("CH3 : "+CH3_Volts);
        notiStyle.addLine("CH4 : "+CH4_Volts);
        notiStyle.addLine("CH5 : "+CH5_Volts);
        notiStyle.addLine("CH6 : "+CH6_Volts);



        PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setTicker("Analog")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Analog")
                .setContentIntent(pi)
                .setAutoCancel(false)
                .setOngoing(true)
                .setStyle(notiStyle)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        startForeground(2, notification);
    }

}