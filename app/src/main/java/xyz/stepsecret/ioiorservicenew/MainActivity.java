package xyz.stepsecret.ioiorservicenew;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity
{


    private BoundService mBoundService;
    private boolean mServiceBound = false;

    private IntentFilter mIntentFilter;

    private List<Double> buf_1 = new ArrayList<Double>();
    private List<Double> buf_2 = new ArrayList<Double>();
    private List<Double> buf_3 = new ArrayList<Double>();
    private List<Double> buf_4 = new ArrayList<Double>();
    private List<Double> buf_5 = new ArrayList<Double>();
    private List<Double> buf_6 = new ArrayList<Double>();

    private LineChart mChart;
    private LineChart mWave;

    private Boolean Check_plot = false;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("velocity");
        mIntentFilter.addAction("ioio_connect");
        mIntentFilter.addAction("ioio_disconnect");
        mIntentFilter.addAction("ioio_loop");

        Inital_1();
        Inital_2();

    }


    public void Inital_1()
    {
        mChart = (LineChart) findViewById(R.id.chart1);
        mChart.setDrawGridBackground(false);

        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);


        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(false);


        mChart.getAxisRight().setEnabled(false);

        XAxis xAxis = mChart.getXAxis();
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelRotationAngle(45);
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {


                return (int)(value * 1000 * 44100 / 2048)+" hz";
            }
        });



        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.setAxisMaximum(16000f);
        leftAxis.setAxisMinimum(0);
        //leftAxis.setYOffset(20f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawGridLines(false);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);

        // dont forget to refresh the drawing
        mChart.invalidate();
    }

    public void Inital_2()
    {
        mWave = (LineChart) findViewById(R.id.wave);
        mWave.setDrawGridBackground(false);

        // no description text
        mWave.getDescription().setEnabled(false);

        // enable touch gestures
        mWave.setTouchEnabled(true);

        // enable scaling and dragging
        mWave.setDragEnabled(true);
        mWave.setScaleEnabled(true);


        // if disabled, scaling can be done on x- and y-axis separately
        mWave.setPinchZoom(false);


        mWave.getAxisRight().setEnabled(false);

        XAxis xAxis = mWave.getXAxis();
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelRotationAngle(45);
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {


                return (int)(value*1000)+"";
            }
        });



        YAxis leftAxis = mWave.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.setAxisMaximum(16000f);
        leftAxis.setAxisMinimum(-16000f);
        //leftAxis.setYOffset(20f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawGridLines(false);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);

        // dont forget to refresh the drawing
        mWave.invalidate();
    }


    @Override
    public void onStart() {
        super.onStart();

        Log.e(" >>>> "," onstart");
        Intent intent = new Intent(this, BoundService.class);
        startService(intent);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);


    }

    @Override
    public void onResume() {
        super.onResume();

        registerReceiver(mReceiver, mIntentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mServiceBound) {
            unbindService(mServiceConnection);
            mServiceBound = false;
        }

    }

    @Override
    public void onPause() {

        super.onPause();
        unregisterReceiver(mReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {

            mServiceBound = false;

        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            BoundService.MyBinder myBinder = (BoundService.MyBinder) service;
            mBoundService = myBinder.getService();
            mServiceBound = true;

        }
    };

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals("ioio_connect"))
            {
                Log.e("Broadcast Receiver ","intent.getAction() : ioio_connect");
            }
            else if(intent.getAction().equals("ioio_disconnect"))
            {
                Log.e("Broadcast Receiver ","intent.getAction() : ioio_connect");
            }
            else if(intent.getAction().equals("ioio_loop"))
            {
                float[] buff_ch1 = intent.getFloatArrayExtra("CH1_Volts");
                float[] buff_ch2 = intent.getFloatArrayExtra("CH2_Volts");
                float[] buff_ch3 = intent.getFloatArrayExtra("CH3_Volts");
                float[] buff_ch4 = intent.getFloatArrayExtra("CH4_Volts");
                float[] buff_ch5 = intent.getFloatArrayExtra("CH5_Volts");
                float[] buff_ch6 = intent.getFloatArrayExtra("CH6_Volts");

                for(int i = 0 ; i < intent.getFloatArrayExtra("CH1_Volts").length; i++)
                {
                    Log.e("xxx"," i : "+i+" >> buf_1 : "+buff_ch1[i]);
                }

                //Log.e("xxx"," buf_1 : "+intent.getFloatExtra("CH6_Volts",0)+x*5);
            }

            // Log.e("BroadcastReceiver ","intent.getAction() : "+intent.getAction());
        }
    };


    private void show2(short[] data)
    {
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        ArrayList<Entry> yVals_wave = new ArrayList<Entry>();

        int size = data.length;
        double[] real = new double[size];
        double[] image = new double[size];

        for (int i = 0; i < size; i++) {

            real[i] = (double) data[i] / 32768.0; // signed 16 bit
            //real[i] = (double) data[i]; // signed 16 bit
            image[i] = 0.0;
            yVals_wave.add(new Entry(i * 0.001f, (float) data[i]));
        }
        //Log.e(">> ","1 real[0] : "+real[0]);
        FFT.transform(real, image);
        //Log.e(">> ","2 real[0] : "+real[0]);

        int new_size = size / 2;
        short[] data_temp = new short[new_size];
        short max_amp = 0;
        int num = 0;
        for(int i = 0 ; i < new_size ; i++)
        {

            data_temp[i] = (short) (Math.sqrt(real[i]*real[i] + image[i]*image[i]) * 100); // Enlarge 100 times
            yVals.add(new Entry(i * 0.001f, (float) data_temp[i]));
            if(max_amp < data_temp[i])
            {
                max_amp = data_temp[i];
                num = i;
            }
        }
        //Log.e(">> ","max_amp : "+max_amp+" freq : "+(num * 44100 / 1792 )+" hz");
        //Log.e(">> ","size : "+size);


        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "DataSet 1");
        LineDataSet set2 = new LineDataSet(yVals_wave, "DataSet 2");

        set1.setColor(Color.BLACK);
        set1.setLineWidth(0.5f);
        set1.setDrawValues(false);
        set1.setDrawCircles(false);
        set1.setMode(LineDataSet.Mode.LINEAR);
        set1.setDrawFilled(false);

        set2.setColor(Color.BLACK);
        set2.setLineWidth(0.5f);
        set2.setDrawValues(false);
        set2.setDrawCircles(false);
        set2.setMode(LineDataSet.Mode.LINEAR);
        set2.setDrawFilled(false);

        // create a data object with the datasets
        LineData data1 = new LineData(set1);
        LineData data2 = new LineData(set2);

        // set data
        mChart.setData(data1);
        mWave.setData(data2);

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();
        l.setEnabled(false);

        Legend l2 = mWave.getLegend();
        l2.setEnabled(false);

    }

}